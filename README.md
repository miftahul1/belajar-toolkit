# Boilerplate
This boilerplate is a template to create a package that will be used on Gredu Web V3

## Features
- nextjs v12
- react-query
- sass
- jest
- setup with conventional-commitizen
- precommit with husky
- minimal setup eslint, prettier, & editorconfig
- yarn v1
- template merge request (feature, issue, default)
- axios
- gredu-toolkit (a internal library that containing components and utilities)

## How to Install
Prerequisite
```
yarn ^v1
node ^v14
```

## Structure Directory
```
example-project
├── pages (Route based)
│   ├── _app.tsx
│   ├── api
│   │   └── hello.ts
│   └── index.tsx
├── public
│   ├── favicon.ico
│   ├── fonts (Global share fonts file.)
│   │   └── Inter
│   │       ├── Inter-Bold.ttf
│   │       ├── Inter-Regular.ttf
│   │       └── Inter-SemiBold.ttf
│   ├── images (Global share assets file)
│   │   └── greduLogo.svg
│   └── vercel.svg
├── src
│   ├── components (Common shared reusable global components)
│   ├── constants (General constant data)
│   ├── hooks (Global shared hooks function)
│   ├── services (Global shared fething resources)
│   │   ├── User
│   │   │   ├── index.ts
│   │   └── index.ts
│   ├── styles (General Mixin css and global variable)
│   │   ├── Home.module.css
│   │   ├── fonts.css
│   │   └── globals.css
│   ├── utils (Utilities purposes)
│   │   ├── axios.ts
│   │   └── index.ts
│   └── views (View Layouts)
│       ├── homepage
│       │   ├── Homepage.tsx
│       │   ├── index.ts
│       │   └── styles.module.scss
│       └── index.ts
```
##
